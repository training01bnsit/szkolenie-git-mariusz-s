﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitSandbox
{
    public class Calculator
    {
        public long Add(int a, int b)
        {
            return a + b;
        }

        public long Subtract(int a, int b)
        {
            return a - b;
        }

        public decimal Multiply(int a, int b)
        {
            return a * b;
        }

        public decimal Divide(int a, int b)
        {
            return a / b;
        }

        public static void Main()
        {
            Console.WriteLine("Hello calculator");
        }
    }
}
